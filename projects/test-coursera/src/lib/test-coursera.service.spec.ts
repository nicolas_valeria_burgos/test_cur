import { TestBed } from '@angular/core/testing';

import { TestCourseraService } from './test-coursera.service';

describe('TestCourseraService', () => {
  let service: TestCourseraService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestCourseraService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
