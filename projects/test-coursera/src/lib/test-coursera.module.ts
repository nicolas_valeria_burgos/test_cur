import { NgModule } from '@angular/core';
import { TestCourseraComponent } from './test-coursera.component';



@NgModule({
  declarations: [TestCourseraComponent],
  imports: [
  ],
  exports: [TestCourseraComponent]
})
export class TestCourseraModule { }
