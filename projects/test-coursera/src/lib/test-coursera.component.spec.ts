import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestCourseraComponent } from './test-coursera.component';

describe('TestCourseraComponent', () => {
  let component: TestCourseraComponent;
  let fixture: ComponentFixture<TestCourseraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestCourseraComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCourseraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
