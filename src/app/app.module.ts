import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { TestCourseraModule } from 'test-coursera';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TestCourseraModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
