import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BoxLoginService {

  private subscribe = new Subject<number>();
  private $subscribe = this.subscribe.asObservable();
  private numberTo = 0;
  constructor(
    private readonly http: HttpClient
  ) {
    let interval = setInterval(() => {
      this.numberTo += 1;
      this.subscribe.next(this.numberTo);
      if (this.numberTo > 5) {
        clearInterval(interval);
      }
    }, 1000)
  }


  getData(data: any) {
    // this.http.get('url', {}).subscribe(
    //   (data) => {
    //     console.log('cuando recibo la data correcto', data);
    //   },
    //   (error) => {
    //     console.log('cuando recibo la data incorrecto', error);
    //   },
    //   () => {
    //     console.log('completado el metodo');
    //   }
    // );
    return this.http.post('url', data).toPromise();
    // .then((data) => console.log('ocurre cuando todo salio bien', data))
    // .catch((error) => console.log('ocurre cuando ocurre un error', error))
    // .finally(() => console.log('Metodo que se ejecuta indistintamente si existe un error o un ok'));
  }


  getDataPromise() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve('todo paso bien');
      }, 2000);
    });
  }

  getDataObservable() {
    return this.$subscribe;
  }

}
