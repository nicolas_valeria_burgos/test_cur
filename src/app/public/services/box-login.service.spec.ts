import { TestBed } from '@angular/core/testing';

import { BoxLoginService } from './box-login.service';

describe('BoxLoginService', () => {
  let service: BoxLoginService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BoxLoginService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
