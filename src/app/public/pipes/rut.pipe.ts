import { Pipe, PipeTransform } from '@angular/core';

import { formatRut } from '../helpers/rut.helper';

/*
 * Formats a string into a valid rut
 * Usage:
 *   rutString | rut
 * Example:
 *   {{ '111111111' | rut}}
 *   formats to: 11.111.111-1
 */

@Pipe({ name: 'rut' })
export class RutPipe implements PipeTransform {
  transform(value: string): string {
    return formatRut(value);
  }
}