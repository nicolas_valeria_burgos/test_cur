import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'appfilter'
})
export class FilterPipe implements PipeTransform {

  transform(value: number[], args: number): number[] {
    return value.filter(e => e >= args);
  }

}
