import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tildes'
})
export class TildesPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): unknown {
    let result = '';
    if (args.length > 0) {
      result = args[0] === 1 ? value.toLowerCase() : value.toUpperCase();
    } else {
      result = 'No tengo parametros'
    }
    return result;
  }

}
