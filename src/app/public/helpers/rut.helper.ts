/**
 *
 * @param rut
 */
export function cleanRut(rut: string): string {
    rut = rut === null ? '' : rut;
    for (let i = 0; i < rut.length;) {
        if (rut.charAt(i) === '0') {
            rut = rut.substring(1, rut.length);
            i = 0;
        } else {
            break;
        }
    }
    return typeof rut === 'string' ? rut.replace(/[^0-9kK]+/g, '').toUpperCase() : '';
}

/**
 *
 * @param rut
 */
export function isValidRut(rut: string): boolean {
    rut = cleanRut(rut);
    const expression = new RegExp('(\\d{6,8})([\\dkK])', 'g');
    if (!expression.test(rut)) {
        return false;
    }
    let t = parseInt(rut.slice(0, -1), 10);
    let m = 0;
    let s = 1;
    while (t > 0) {
        s = (s + ((t % 10) * (9 - (m++ % 6)))) % 11;
        t = Math.floor(t / 10);
    }
    const v = (s > 0) ? `${s - 1}` : 'K';
    if (v === rut.slice(-1)) {
        return true;
    } else {
        return false;
    }
}

/**
 *
 * @param rut
 */
export function formatRut(rut: string): string {
    rut = cleanRut(rut);
    if (rut.length === 0) {
        return '';
    }
    if (rut.length === 1) {
        return rut;
    }
    let result = `${rut.slice(-4, -1)} -${rut.substr(rut.length - 1)}`;
    for (let i = 4; i < rut.length; i += 3) {
        result = `${rut.slice(-3 - i, -i)}.${result}`;
    }
    return result;
}

/**
 *
 * @param rut
 */
export function isRutBusiness(rut: string): boolean {
    rut = rut === null || rut === undefined ? '0' : rut.trim();
    return Number(rut) >= 500000000 && Number(rut) < 2000000000;
}

/**
 *
 * @param rut
 */
export function isRutPerson(rut: string): boolean {
    rut = rut === null || rut === undefined ? '0' : rut.trim();
    return Number(rut) < 500000000 || Number(rut) >= 2000000000;
}