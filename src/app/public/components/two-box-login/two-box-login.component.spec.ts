import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TwoBoxLoginComponent } from './two-box-login.component';

describe('TwoBoxLoginComponent', () => {
  let component: TwoBoxLoginComponent;
  let fixture: ComponentFixture<TwoBoxLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TwoBoxLoginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TwoBoxLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
