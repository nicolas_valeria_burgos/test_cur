import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-two-box-login',
  templateUrl: './two-box-login.component.html',
  styleUrls: ['./two-box-login.component.scss']
})
export class TwoBoxLoginComponent implements OnInit {

  title: any;

  @Input()
  set onTitle(value: boolean) {
    this.title = value ? 'Hola A todos' : new Date();
  };

  constructor() { }

  ngOnInit(): void {
  }

}
