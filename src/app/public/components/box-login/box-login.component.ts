import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-box-login',
  templateUrl: './box-login.component.html',
  styleUrls: ['./box-login.component.scss']
})
export class BoxLoginComponent implements OnInit {

  public title = '';

  @Input()
  set onTitle(value: boolean) {
    this.title = value ? 'Hola A todos' : 'Chao a todos';
  };

  @Output()
  private readonly onClick = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  clickButton(event: any) {
    this.onClick.emit(event);
  }

}
