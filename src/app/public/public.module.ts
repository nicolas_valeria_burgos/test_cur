import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { BoxLoginComponent } from './components/box-login/box-login.component';
import { TwoBoxLoginComponent } from './components/two-box-login/two-box-login.component';
import { BackgroundColorDirective } from './directives/background-color.directive';
import { RutDirective } from './directives/rut.directive';
import { PublicComponent } from './pages/public/public.component';
import { FilterPipe } from './pipes/filter.pipe';
import { RutPipe } from './pipes/rut.pipe';
import { TildesPipe } from './pipes/tildes.pipe';
import { PublicRoutingModule } from './public-routing.module';
import { BoxLoginService } from './services/box-login.service';


@NgModule({
  declarations: [
    PublicComponent,
    BoxLoginComponent,
    TwoBoxLoginComponent,
    BackgroundColorDirective,
    TildesPipe,
    FilterPipe,
    RutDirective,
    RutPipe,
  ],
  imports: [
    CommonModule,
    PublicRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    BoxLoginService
  ]
})
export class PublicModule { }
