export interface Basic {
    id?: string;
    createdAt?: Date;
    updatedAt?: Date;
}
