import { Basic } from './basic';

export interface Person extends Basic {
    name: string;
    edad: number;
    lastname: string;
}
