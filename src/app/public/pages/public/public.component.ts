import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { formatRut } from '../../helpers/rut.helper';
import { Person } from './../../interfaces/person';
import { Persona } from './../../interfaces/persona.model';
import { BoxLoginService } from './../../services/box-login.service';

@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: ['./public.component.scss']
})
export class PublicComponent implements OnInit {

  public valueTitle = true;
  public filter = 0;
  public rutInsert = '';
  public arregloNumeros = [0, 1, 2, 3, 4, 5];

  private variable = 1;
  public variablePublc = 2;

  private readonly variablePrivate: number = 1;
  public readonly variablePublic = 2;

  constructor(
    private readonly router: Router,
    private readonly boxLogin: BoxLoginService
  ) {
  }

  async ngOnInit(): Promise<void> {
    const persona: Person = {
      name: 'asd',
      edad: 5,
      lastname: 'asdad',
      id: 'adkajdlka'
    }

    const asdasda = new Persona('asdasd', 39, 'asdasda');
    console.log('persona', persona);
    console.log('asdasda', asdasda);
    console.log(formatRut('182738301'));
    // this.boxLogin.getDataPromise()
    //   .then((data) => console.log('data de la promesa', data))
    //   .finally(() => console.log('finalizo la promesa'));
    // this.boxLogin.getDataObservable().toPromise()
    //   .then((data) => console.log('data del obsevable como promesa', data))
    //   .finally(() => console.log('finalizo el observable como promesa'));
    console.log('async 1');
    try {
      const response = await this.boxLogin.getData(asdasda);
    } catch (error) {
      console.log('error de promesa', error);
    } finally {
      console.log('async 1 finally');
    }
    console.log('async 2');
    console.log('1');
    this.boxLogin.getData(asdasda).then(() => console.log('bien')).catch(() => console.log('error')).finally(() => console.log('1 finally'))
    console.log('2');

  }

  recibirData($event: any) {
    console.log('soy el padre', $event);
    this.valueTitle = !this.valueTitle;
  }

  irPrivate() {
    this.router.navigate(['/private']);
  }

}
