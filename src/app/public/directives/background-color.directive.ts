import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appBackgroundColor]'
})
export class BackgroundColorDirective {

  constructor() { }

  @HostListener('click')
  onClick() {
    console.log('me hicieron click');
  }

}
