import { Directive, HostListener } from '@angular/core';

import { cleanRut, formatRut } from '../helpers/rut.helper';

@Directive({
  selector: '[appRut]'
})
export class RutDirective {

  constructor() { }

  @HostListener('blur', ['$event']) onBlur(ev: Event) {
    const htmlInputElement: HTMLInputElement = ev.target as HTMLInputElement;
    htmlInputElement.value = formatRut(htmlInputElement.value) || '';
  }

  @HostListener('focus', ['$event']) onFocus(ev: Event) {
    const htmlInputElement: HTMLInputElement = ev.target as HTMLInputElement;
    htmlInputElement.value = cleanRut(htmlInputElement.value);
  }

  @HostListener('keydown', ['$event'])
  onKeydown(ev: KeyboardEvent) {
    let code = Number(ev.code);
    // if (ev.keyCode !== undefined) {
    //   code = ev.keyCode;
    // } else if (ev.which !== undefined) {
    //   code = ev.which;
    // }

    if ([46, 8, 9, 27, 13, 110, 190].indexOf(code) !== -1 ||
      // Allow: Ctrl+A
      (code === 65 && (ev.ctrlKey || ev.metaKey)) ||
      // Allow: Ctrl+C
      (code === 67 && (ev.ctrlKey || ev.metaKey)) ||
      // Allow: Ctrl+V
      (code === 86 && (ev.ctrlKey || ev.metaKey)) ||
      // Allow: Ctrl+X
      (code === 88 && (ev.ctrlKey || ev.metaKey)) ||
      // Allow: home, end, left, right
      (code >= 35 && code <= 39)) {
      // let it happen, don't do anything
      return;
    } else if ((/[^0-9kK]+/g.test(ev.key))) {
      ev.stopPropagation();
      ev.preventDefault();
    }
  }

  @HostListener('drop', ['$event']) onDrop(ev: DragEvent) {
    if (ev && ev.dataTransfer) {
      const value = ev.dataTransfer.getData('text');
      if (/[^0-9kK]+/g.test(value)) {
        ev.stopPropagation();
        ev.preventDefault();
      }
    }
  }
}