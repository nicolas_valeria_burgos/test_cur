import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AllPagesComponent } from './pages/all-pages/all-pages.component';
import { PrivateComponent } from './pages/private/private.component';

const routes: Routes = [
  {
    path: '',
    component: PrivateComponent
  },
  {
    path: 'all-pages',
    component: AllPagesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrivateRoutingModule { }
