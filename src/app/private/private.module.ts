import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TestCourseraModule } from 'test-coursera';

import { AllPagesComponent } from './pages/all-pages/all-pages.component';
import { PrivateComponent } from './pages/private/private.component';
import { PrivateRoutingModule } from './private-routing.module';


@NgModule({
  declarations: [PrivateComponent, AllPagesComponent],
  imports: [
    CommonModule,
    PrivateRoutingModule,
    TestCourseraModule
  ]
})
export class PrivateModule { }
